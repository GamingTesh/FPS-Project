using Godot;
using System;

public class GlobalSignals : Node
{
    [Signal]
    public delegate void bullet_fired(Bullet bullets, int team,Position2D position, Vector2 direction);
}
