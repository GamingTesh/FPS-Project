using Godot;
using System;

public class PickupItem : RigidBody
{
    [Export]
    private int ItemHoldDistance = 5;

    public int GetItemHoldDistance()
    {
        return ItemHoldDistance;
    }
}
