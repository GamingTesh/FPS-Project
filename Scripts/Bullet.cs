using Godot;
using System;

public class Bullet : RigidBody
{
    [Export]
    public int speed = 1;

    Vector3 direction = Vector3.Zero;

    private AnimationPlayer animation_player;

    public override void _Ready()
    {
        animation_player = GetNode<AnimationPlayer>("AnimationPlayer");
        animation_player.Play("shot");
    }

    public override void _PhysicsProcess(float delta)
    {
        if(direction != Vector3.Zero)
        {
           this.ApplyCentralImpulse(direction * speed);
        }
    }

    public void Set_Direction(Vector3 direction)
    {
        this.direction = direction;
    }

    public void _on_Area_body_entered(Node body)
    {
        //if(body.IsInGroup("object"))
            //QueueFree();
    }

    public void _on_DestroyTimer_timeout()
    {   
        QueueFree();
    }
}
