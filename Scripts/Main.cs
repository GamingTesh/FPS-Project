using Godot;
using System;

public class Main : Spatial
{
    Spatial bullet_manager;
    KinematicBody player;

    public override void _Ready()
    {
        bullet_manager = GetNode<Spatial>("BulletManager");
        player = GetNode<KinematicBody>("Player");

        var global_signal = (GlobalSignals)GetNode("/root/GlobalSignals");
        global_signal.Connect("bullet_fired", bullet_manager, "handle_bullet_spawned");
    }
}
