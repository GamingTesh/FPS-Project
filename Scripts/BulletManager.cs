using Godot;
using System;

public class BulletManager : Spatial
{
    public void handle_bullet_spawned(Bullet bullet, Position3D position, Vector3 direction)
    {
        AddChild(bullet);
        bullet.GlobalTransform = position.GlobalTransform;
        bullet.Set_Direction(direction);
    }
}
