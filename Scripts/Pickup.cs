using Godot;
using System;

public class Pickup : Spatial
{

    PickupItem grabbed_object = null;

    [Export]
    private int object_throw_force = 120;

    [Export]
    private int object_grab_ray_distance = 10;

    Camera camera;

    public void init(Camera _camera)
    {
        camera = _camera;
    }

    public override void _PhysicsProcess(float delta)
    {
        ProcessInput(delta);
    }

    void ProcessInput(float delta)
    {
        if(Input.IsActionJustPressed("throw_object"))
        {
            if(grabbed_object == null)
            {
                var state = GetWorld().DirectSpaceState;

                var center_position = GetViewport().Size / 2;
                var ray_from = camera.ProjectRayOrigin(center_position);
                var ray_to = ray_from + camera.ProjectRayNormal(center_position) * object_grab_ray_distance;

                var ray_result = state.IntersectRay(ray_from, ray_to, new Godot.Collections.Array{this});
                if(ray_result.Count != 0)
                {
                    if(ray_result["collider"] is PickupItem)
                    {
                        grabbed_object = (PickupItem)ray_result["collider"];
                        grabbed_object.Mode = RigidBody.ModeEnum.Static;
                    }
                }
            }
            else
            {

                if(Input.IsActionJustPressed("drop_object"))
                {
                    
                    grabbed_object.Mode = RigidBody.ModeEnum.Rigid;

                    grabbed_object.ApplyImpulse(new Vector3(0, 0, 0), -camera.GlobalTransform.basis.z.Normalized());
                    grabbed_object = null;

                }
                else
                {
                    
                    grabbed_object.Mode = RigidBody.ModeEnum.Rigid;

                    grabbed_object.ApplyImpulse(new Vector3(0, 0, 0), -camera.GlobalTransform.basis.z.Normalized() * object_throw_force);
                    grabbed_object = null;
                }

            }

        }

        if(grabbed_object != null)
        {
            var transform = grabbed_object.GlobalTransform;
            transform.origin = camera.GlobalTransform.origin + (-camera.GlobalTransform.basis.z.Normalized() * grabbed_object.GetItemHoldDistance());
            grabbed_object.GlobalTransform = transform;
        }
            
    }

    public PickupItem GetGrabbedObject()
    {
        return grabbed_object;
    }

}
